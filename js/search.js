
// Вывод формы поиска
function showSearch() {
    var form = document.getElementsByTagName('input')[0];

    if(!form.classList.length)
    {
        if(document.documentElement.clientWidth < 1100 )
        {
            document.getElementsByClassName('sign')[0].style.display ='none';
        }

        form.classList.add('hover');
    }
    else
    {
        if(document.documentElement.clientWidth < 1180 )
        {
            document.getElementsByClassName('sign')[0].removeAttribute('style');
        }
        form.classList.remove('hover');
    }

}

// Бургер меню
function menu()
{
    if(document.getElementsByClassName('menu-toggle')[0].classList.length === 1)
    {

        document.getElementsByClassName('menu-toggle')[0].classList.add('open');
        document.getElementsByClassName('min-menu')[0].classList.add('open');
    }
    else
    {
        document.getElementsByClassName('menu-toggle')[0].classList.remove('open');
        document.getElementsByClassName('min-menu')[0].classList.remove('open');

    }
}


// Отключаем бургер меню при достаточном увеличение экрана и обнуляем бургер меню 
window.addEventListener("resize", function(event) {
    

    if(document.documentElement.clientWidth < 1100 && document.getElementsByTagName('input')[0].classList.length > 0 )
    {
        
        document.getElementsByClassName('sign')[0].style.display = 'none';
    } 
    else
    {
        document.getElementsByClassName('sign')[0].removeAttribute('style');
    }

    if(document.documentElement.clientWidth > 700 && document.getElementsByClassName('menu-toggle')[0].classList.length > 1 )
    {   
        document.getElementsByClassName('menu-toggle')[0].classList.remove('open');
        document.getElementsByClassName('min-menu')[0].classList.remove('open');
        document.getElementsByClassName('min-menu')[0].style.display = 'none';
    }
    else
    {
        document.getElementsByClassName('min-menu')[0].removeAttribute('style');
    }
  });